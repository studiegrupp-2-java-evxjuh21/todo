const toDoArray = [];

const addToDo = () => {
    const toDoValue = document.getElementById("todo-input").value;
    const toDoPriority = parseInt(document.getElementById("setPriority").value);
    toDoArray.push({
        todo: toDoValue,
        priority: toDoPriority
    });

    renderToDoToHTML();
    document.getElementById("todo-input").value = "";
}

const renderToDoToHTML = () => {
    document.querySelector(".toDo-list").innerHTML = ""; //`<ul class="toDo-list"></ul>`;

    toDoArray.sort(function(a, b) { return a.priority - b.priority });

    toDoArray.forEach((todo, index) => {
        const li = document.createElement("li");
        const h2 = document.createElement("h2");
        const removeBtn = document.createElement("button");
        removeBtn.classList.add("removeBtn");
        removeBtn.append("X");
        removeBtn.onclick = function () {removeTodo(li, index)};
        h2.append("Aktivitet:");
        li.append(h2, todo.todo, removeBtn);
        colorPriority(li, todo.priority);
        document.querySelector(".toDo-list").append(li);
    });
}

const colorPriority = (li, priority) => {
    switch (priority) {
        case 1:
            return li.classList.add("todo-priority-one");
        case 2:
            return li.classList.add("todo-priority-two");
        case 3:
            return li.classList.add("todo-priority-three");
    }
}

const toDoCheck = document.querySelector('.toDo-list');

toDoCheck.addEventListener('click', (event) => {
    if (event.target.tagName === "LI" || event.target.tagName === "H2") {

        if (event.target.style.textDecoration === "line-through") {

            event.target.style.textDecoration = "none"
            event.target.classList.remove('check-status');

        } else {
            //document.querySelector('H2')[0].style.textDecoration = "none";
            event.target.style.textDecoration = "line-through";
            event.target.classList.add('check-status');

        }
    }
});

const removeTodo = (li, index) => {
    toDoArray.splice(index, 1);
    renderToDoToHTML();
}